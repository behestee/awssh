# To access server via ssh limit access to pem file
```shell
chmod 400 .ssh/filename.pem 
```

# Connect to server through ssh
```shell
ssh -i .ssh/filename.pem ubuntu@servername
```

# Download letsencript for free SSL
```shell
sudo git clone https://github.com/letsencrypt/letsencrypt /opt/letsencrypt
```

# Fix
```shell
export LC_ALL="C"
```


# Goto 
```shell
cd /opt/letsencrypt
```

# Run letsencript to generate ssl certificate for this domain
```shell
./letsencrypt-auto certonly --standalone --email user@example.com -d your.domain.com
```

### Run letsencript to renew in the future
```shell
./letsencrypt-auto renew
```
> Run before ending 3 months or the date mentioned after certificate creation.  Before run you need to
> Stop all running server who is ocupying 80 port.
```shell
# To stop 
sudo systemctl stop nginx

# To start 
sudo systemctl start nginx

# To see the status 
sudo systemctl status nginx

# Also you can check any other app using this port or not 
netstat -na | grep ':80.*LISTEN'
```

# To see files were created or not
```shell
sudo ls /etc/letsencrypt/live/your.domain.com
```

# Prepare for serversetup
```shell
cd ~
git clone https://bitbucket.org/behestee/awssh.git
cd awssh
chmod +x aws.sh
sudo ./aws.sh
```
> It will create and configure your server, just you need to modify the vhost conf, Jenkins configuration 
> and create your job at there. It also will install the mysql secure installation script and prompt you 
> for give your settings. 

# Modify Domain Name as necessary
```shell
sudo vim  /etc/nginx/sites-available/nginx_vhost
sudo vim /etc/hosts
```

# Administer MongoDB
```shell
mongo
use admin
db.createUser({user:"admin", pwd:"secret", roles:[{role:"root", db:"admin"}]})
exit
```


# Restart nginx server

```shell
sudo service nginx restart
```

# Know nginx status

```shell
sudo service nginx status
```

# Get First Password for Jenkins and Configure for Project
```shell
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```


